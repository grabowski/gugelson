var path = require("path");
var webpack = require("webpack");
module.exports = [
	{
		name: "gugelson",
		entry: "./src/index.js",
		output: {
			path: path.join(__dirname, "dist"),
			filename: "gugelson.js",
				libraryTarget:'umd',
		library:'gugelson'
		},
		module: {
			loaders: [
				{
					test: /\.js$/, loader: "babel"
				}

			]
		}
	}
];
