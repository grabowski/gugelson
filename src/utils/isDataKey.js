import config from '../config';

const isDataKey = (key) => {
	return key.includes(config.DATA_KEY_PREFIX);
}
export default isDataKey;
