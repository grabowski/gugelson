import config from '../config';
const cleanKeyName = (key) => key.replace(config.DATA_KEY_PREFIX, "");
export default cleanKeyName;
