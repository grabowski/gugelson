import getRawData from './getRawData';
import cleanKeyName from './cleanKeyName';
import isDataKey from './isDataKey';


module.exports = {
	getRawData,
	cleanKeyName,
	isDataKey
}
