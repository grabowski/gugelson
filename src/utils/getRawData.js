const getRawData = (data)=>{
		let normalizedData;

	normalizedData = (typeof data === 'string') ? JSON.parse(data) : data;
	return normalizedData.feed.entry;
};

export default getRawData;

