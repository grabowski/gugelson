import utils from './utils';
import toArray from './parsers/toArray';

const gugelson = {
	utils,
	parseToArray:toArray
}
module.exports = gugelson;

