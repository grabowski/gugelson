import config from '../../config';
import { getRawData, isDataKey, cleanKeyName } from '../../utils';

export default (input) => {
		let rawData = getRawData(input);
		const cleanData = rawData.map(function (item, index) {
		
		let result = {};
		Object.keys(item).forEach(key => {
			if (isDataKey(key)) {
				result[cleanKeyName(key)] = item[key][config.DATA_ENTRY_VALUE_KEY];
			}

		});
			return result;
		});
	return cleanData;

}

